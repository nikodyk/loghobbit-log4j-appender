# README #

### License ###

The Log4J appender for LogHobbit falls under the MIT lencense.

### What is this repository for? ###

This is the LogHobbit appender for Log4J, allowing java applications to leverage a simple integration into a log management system.

### How do I get set up? ###

* Clone repository
* build with maven using command 'mvn clean install'

If Running Locally (or running the tests)
* Create a user on your local platform
* Create a group and modify its access token to be 55b59bb28902680c7ad230ea
* Verify communication by ensuring there are no deserialization errors from the service and no errors in the console log

### Contribution guidelines ###

* When contributing, make a branch.  When your feature is complete make a pull request
* All system tests should use the token specified above
* Ensure that all tests pass before making a pull request.  Commenting out test cases will cause the pull request to become immediatly rejected

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact