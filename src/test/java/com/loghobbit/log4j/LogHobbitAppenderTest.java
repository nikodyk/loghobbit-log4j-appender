package com.loghobbit.log4j;

import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loghobbit.entities.LogEntry;

public class LogHobbitAppenderTest {
	private static final String SERVER = "http://api.loghobbit.com/v1/logs";
	private static final String ACCESS_KEY = "1234";
	private static final String CATEGORY_CLASS = "LogHobbitAppenderTest";
	@SuppressWarnings("deprecation")
	private static final Category CATEGORY = Category.getInstance(LogHobbitAppenderTest.class);
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	private LogHobbitAppender appender = new LogHobbitAppender();
	private HttpClient client;
	private HttpUriRequest request;
	private HttpResponse response;
	private StatusLine statusLine;
	private HttpEntity entity;
	
	@Before
	public void setup() throws ClientProtocolException, IOException
	{
		this.entity = mock(HttpEntity.class);
		
		this.statusLine = mock(StatusLine.class);
		when(this.statusLine.getStatusCode()).thenReturn(200);
		
		this.response = mock(HttpResponse.class);
		when(this.response.getStatusLine()).thenReturn(this.statusLine);
		when(this.response.getEntity()).thenReturn(this.entity);
		
		this.client = mock(HttpClient.class);
		when(this.client.execute(any(HttpUriRequest.class))).thenReturn(this.response);
		when(this.client.execute(any(HttpUriRequest.class))).thenAnswer(new Answer<HttpResponse>(){

			@Override
			public HttpResponse answer(InvocationOnMock invocation) throws Throwable {
				request = invocation.getArgumentAt(0, HttpUriRequest.class);
				return response;
			}
			
		});
		
		this.appender.setHttpClient(this.client);
		this.appender.setServer(SERVER);
		this.appender.setAccessKey(ACCESS_KEY);
		this.appender.setIsAsync(false);
		this.appender.setName("Test Appender");
	}
	
	@Test
	public void logString() throws Exception
	{
		this.appender.append(new LoggingEvent(CATEGORY_CLASS, CATEGORY, 0, Level.ERROR, "Message", null));
		
		Assert.assertNotNull(this.request);
		LogEntry event = this.getLogEvent(this.request);
		Assert.assertNotNull(event);
		Assert.assertEquals("Message", "Message", event.getMessage());
		Assert.assertNull(event.getException());
		Assert.assertEquals("Access Key", this.appender.getAccessKey(), event.getAccessKey());
		Assert.assertNotNull("Date", event.getDate());
		Assert.assertEquals(com.loghobbit.entities.Level.error, event.getLevel());
	}
	
	@Test
	public void logException() throws Exception
	{
		try
		{
			throw new Exception("Test Exception");
		}
		catch(Exception e)
		{
			this.appender.append(new LoggingEvent(CATEGORY_CLASS, CATEGORY, 0, Level.ERROR, e, null));
		}
		
		Assert.assertNotNull(this.request);
		LogEntry event = this.getLogEvent(this.request);
		Assert.assertNotNull(event);
		Assert.assertEquals("Message", "Test Exception", event.getMessage());
		Assert.assertNotNull(event.getException());
		Assert.assertEquals("Access Key", this.appender.getAccessKey(), event.getAccessKey());
		Assert.assertNotNull("Date", event.getDate());
		Assert.assertEquals(com.loghobbit.entities.Level.error, event.getLevel());
	}
	
	@Test
	public void logMessageAndException() throws Exception
	{
		try
		{
			throw new Exception("Test Exception");
		}
		catch(Exception e)
		{
			this.appender.append(new LoggingEvent(CATEGORY_CLASS, CATEGORY, 0, Level.ERROR, "This is a message", e));
		}
		
		Assert.assertNotNull(this.request);
		LogEntry event = this.getLogEvent(this.request);
		Assert.assertNotNull(event);
		Assert.assertEquals("Message", "This is a message", event.getMessage());
		Assert.assertNotNull(event.getException());
		Assert.assertEquals("Access Key", this.appender.getAccessKey(), event.getAccessKey());
		Assert.assertNotNull("Date", event.getDate());
		Assert.assertEquals(com.loghobbit.entities.Level.error, event.getLevel());
	}
	
	@Test
	public void logMessageAndExceptionWithInnerException() throws Exception
	{
		try
		{
			try
			{
				throw new Exception("Inner Test Exception");
			}
			catch(Exception e)
			{
				throw new Exception("Test Exception", e);
			}
		}
		catch(Exception e)
		{
			this.appender.append(new LoggingEvent(CATEGORY_CLASS, CATEGORY, 0, Level.ERROR, "This is a message", e));
		}
		
		Assert.assertNotNull(this.request);
		LogEntry event = this.getLogEvent(this.request);
		Assert.assertNotNull(event);
		Assert.assertEquals("Message", "This is a message", event.getMessage());
		Assert.assertNotNull(event.getException());
		Assert.assertTrue("Inner exception", event.getException().contains("Inner Test Exception"));
		Assert.assertEquals("Access Key", this.appender.getAccessKey(), event.getAccessKey());
		Assert.assertNotNull("Date", event.getDate());
		Assert.assertEquals(com.loghobbit.entities.Level.error, event.getLevel());
	}
	
	private LogEntry getLogEvent(HttpUriRequest request) throws JsonParseException, JsonMappingException, UnsupportedOperationException, IOException
	{
		HttpPut put = (HttpPut)request;
		
		Assert.assertEquals("Request Entity Class", StringEntity.class, put.getEntity().getClass());
		Assert.assertEquals("Entity Type", StringEntity.class, put.getEntity().getClass());
		StringEntity entity = (StringEntity)put.getEntity();
		String data = IOUtils.toString(entity.getContent());
		return mapper.readValue(data, LogEntry.class);
	}
}
