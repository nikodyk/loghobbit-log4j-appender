package com.loghobbit.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogEntry {
	private String accessKey;
	public String getAccessKey(){ return this.accessKey; }
	public void setAccessKey(String value){ this.accessKey = value; }
	
	private Date date;
	public Date getDate(){ return this.date; }
	public void setDate(Date value)
	{ 
		this.date = value;
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		this.simpleDate = format.format(value);
	}
	
	private String simpleDate;
	public String getSimpleDate(){ return this.simpleDate; }
	public void setSimpleDate(String value){}
	
	private String logName;
	public String getLogName(){ return this.logName; }
	public void setLogName(String value){ this.logName = value; }

	private String server;
	public String getServer(){ return this.server; }
	public void setServer(String value){ this.server = value; }

	private Level level;
	public Level getLevel(){ return this.level; }
	public void setLevel(Level value){ this.level = value; }

	private String message;
	public String getMessage(){ return this.message; }
	public void setMessage(String value){ this.message = value; }

	private String exception;
	public String getException(){ return this.exception; }
	public void setException(String value){ this.exception = value; }
	
	public LogEntry()
	{
	}
	
	public LogEntry(String logName, String server, Level level, String message, String exception)
	{
		this.setDate(new Date());
		this.setLevel(level);
		this.setLogName(logName);
		this.setMessage(message);
		this.setServer(server);
		this.setException(exception);
	}
}
