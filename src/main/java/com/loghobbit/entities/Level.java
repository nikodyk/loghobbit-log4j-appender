package com.loghobbit.entities;

public enum Level {
	info,
	warn,
	error,
	fatal
}
