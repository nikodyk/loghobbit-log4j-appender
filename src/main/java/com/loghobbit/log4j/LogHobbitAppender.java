package com.loghobbit.log4j;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loghobbit.entities.Level;
import com.loghobbit.entities.LogEntry;

@SuppressWarnings("deprecation")
public class LogHobbitAppender extends AppenderSkeleton implements Runnable {
	private static final String UNKNOWN_HOST = "unknown";
	//private static final String KEY = "access_key";
	private static final ObjectMapper mapper = new ObjectMapper();

	
	private String hostName;
	
	/**
	 * The http client used to communicate with the server
	 */
	private HttpClient client;
	public void setHttpClient(HttpClient value){ this.client = value; }
	
	/**
	 * the access key for the log group
	 */
	private String accessKey;
	public String getAccessKey(){ return this.accessKey; }
	public void setAccessKey(String value){ this.accessKey = value; }
	
	/**
	 * The url where to put the log message
	 */
	private String server;
	public String getServer(){ return this.server; }
	public void setServer(String value){ this.server = value; }
	
	private boolean isAsync = true;
	public boolean getIsAsync(){ return this.isAsync; }
	public void setIsAsync(boolean value){ this.isAsync = value; }
	
	private Queue<LoggingEvent> eventQueue = new LinkedList<LoggingEvent>();
	private Thread thread;
	
	public LogHobbitAppender()
	{
		this.closed = false;
		
		this.client = new DefaultHttpClient();
		
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();

			if (ip.getHostName() != null && ip.getHostName().length() > 0) {
				this.hostName = ip.getHostName();
			} 
			else 
			{
				this.hostName = ip.getHostAddress();
			}
			System.out.println("LogHobbit: Current IP address : " + ip.getHostAddress());

		} catch (UnknownHostException e) {
			this.hostName = UNKNOWN_HOST;
		}
		
		this.thread = new Thread(this);
		this.thread.setName("LogHobbit Appender");
		this.thread.start();
	}
	
	public void close() {
		this.closed = true;
	}

	public boolean requiresLayout() {
		return false;
	}

	private void initialize()
	{
		
	}
	
	@Override
	protected void append(LoggingEvent event) {
		if(this.isAsync)
		{
			synchronized(this.eventQueue)
			{
				if(this.eventQueue.size() > 100)
				{
					return;
				}
				this.eventQueue.add(event);
			}
		}
		else
		{
			this.processEvent(event);
		}
	}
	
	@Override
	public void run()
	{
		while(!this.closed)
		{
			LoggingEvent event = null;
			
			synchronized(this.eventQueue)
			{
				if(this.eventQueue.size() > 0)
				{
					event = this.eventQueue.poll();
				}
			}
			
			if(event != null)
			{
				try
				{
					this.processEvent(event);
				} catch(Throwable e) {
					System.out.println("LogHobbit: An error occured while sending the message to send to LogHobbit" + e.toString());
				}
			}
			else
			{
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					
				}
			}
		}
	}
	
	private void processEvent(LoggingEvent event)
	{
		if(this.client == null)
		{
			this.initialize();
		}
		LogEntry entry = new LogEntry();
		entry.setLevel(Enum.valueOf(Level.class, event.getLevel().toString().toLowerCase()));

		if (event.getMessage() != null) {
			if (Throwable.class.isInstance(event.getMessage())) {
				entry.setMessage(((Throwable) event.getMessage()).getMessage());
				entry.setException(this.exceptionToString((Throwable) event.getMessage()));
			} else {
				entry.setMessage(event.getMessage().toString());
			}
		}

		if (event.getThrowableInformation() != null) {
			entry.setException(exceptionToString((Throwable) event
					.getThrowableInformation().getThrowable()));
		}

		entry.setLogName(event.getLoggerName());
		entry.setServer(this.hostName);
		entry.setDate(new Date(event.getTimeStamp()));
		entry.setAccessKey(this.accessKey);
		
		HttpPut put = new HttpPut(this.server);
		try {
			String data = mapper.writeValueAsString(entry);
			put.setEntity(new StringEntity(data));
		} catch (UnsupportedEncodingException e) {
			System.out.println("LogHobbit: An error occured while constructing the message to send to LogHobbit" + e.toString());
			return;
		} catch (JsonProcessingException e) {
			System.out.println("LogHobbit: An error occured while constructing the message to send to LogHobbit" + e.toString());
			return;
		}
		
		put.addHeader("Accept", "application/json");
		put.addHeader("Content-Type", "application/json");
        
		try {
			HttpResponse response = this.client.execute(put);
			
			if(response.getStatusLine().getStatusCode() != 200)
			{
				System.out.println("LogHobbit: An error occured while sending the message " + response.getStatusLine().getStatusCode());
			}
			
			response.getEntity().consumeContent();
		} catch (ClientProtocolException e) {
			System.out.println("LogHobbit: An error occured while sending the message to send to LogHobbit" + e.toString());
			return;
		} catch (IOException e) {
			System.out.println("LogHobbit: An error occured while sending the message to send to LogHobbit" + e.toString());
			return;
		}
	}

	private String exceptionToString(Throwable e) {
		StringBuilder retval = new StringBuilder();
		retval.append(e.getMessage());
		retval.append("\n");
		for (StackTraceElement stack : e.getStackTrace()) {
			retval.append(String.format("%s.%s - %d\n", stack.getClassName(),
					stack.getMethodName(), stack.getLineNumber()));
		}
		
		if(e.getCause() != null)
		{
			retval.append("\n");
			retval.append(this.exceptionToString(e.getCause()));
		}

		return retval.toString();
	}
}
